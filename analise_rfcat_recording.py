#!/usr/bin/python3

import statistics as stat
from itertools import chain
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as si

filt_len = 30 # Found by iterating and plotting waves
one_width = 40 # Approximate width of one sample
payload_len = 25

with open('rf_meas/100x.dat') as file:
    content = file.read()

preamble = [1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0]
plt.plot([2, 2, 0, 0, 0, 0, 0, 0, 0] + preamble)
plt.title('Start of a Transmission')
plt.xlabel('Samples')
plt.ylabel('Output Value')
plt.savefig('plots/preamble.png')
plt.close()
preamble_sym = [2.0 * (elem - 0.5 ) for elem in preamble]

# Reconstruct waveform from characters
waves = [[int(digit) for digit in list(''.join(["{0:08b}".format(int('0x' + char, 0)) for char in line]))] for n, line in enumerate(content.split('\n')) if n != 0 and line]

# First simple filtering
waves_filtered = [si.lfilter(np.ones(filt_len)/filt_len, [1], wave) for wave in waves]
# Normalising filtering
waves_filtered = [wave / np.std(wave) for wave in waves_filtered]
# Create a one sample transition and do a hard decision
waves_rect = [si.medfilt([1.0 if sample > 1 else 0 for sample in wave], int(filt_len / 3 * 2 + 1)) for wave in waves_filtered]
# Check where transitions happen
transitions_idxs = [[idx for idx, sample in enumerate(np.diff(wave)) if sample != 0] for wave in waves_rect]
# Calculate widths of transitions
transitions_widths = [np.diff(wave) for wave in transitions_idxs]
# Create histograms to find pulse widths
all_delays = list(chain.from_iterable(transitions_widths))
plt.hist(all_delays)
plt.title('Histogram of All Delays')
plt.xlabel('Delay in Sample')
plt.ylabel('Amount of Samples with that Delay')
plt.savefig('plots/delay_hist_all.png')
plt.close()
plt.hist(all_delays, bins=20, range=(25, 100))
plt.title('Histogram of Filtered (25 to 100 samples) Delays')
plt.xlabel('Delay in Sample')
plt.ylabel('Amount of Samples with that Delay')
plt.savefig('plots/delay_hist_filtered.png')
plt.close()
one_width = stat.mean([delay for delay in all_delays if delay < 60 and delay >= 25])
print('Mean width of short pulses: ' + str(one_width))
print('Mean width of long pulses: ' + str(stat.mean([delay for delay in all_delays if delay >= 60 and delay < 100])))
# Create normalised width of pulses (values of pulses TBD)
width_normalised = [[int(round(trans_width / int(one_width))) for trans_width in trans_widths] for trans_widths in transitions_widths]
# Find sampling index in the middle of two transitions
sample_points = [[int((trans + (transitions_idx[idx + 1])) / 2) for idx, trans in enumerate(transitions_idx[:-1])] for transitions_idx in transitions_idxs]
# Sample signal value in the middle of two transitions
sampled_values = [[int(waves_rect[idx][point]) for point in point_train] for idx, point_train in enumerate(sample_points)]
# Combine sampled values with pulse width
decoded = [list(chain.from_iterable([[val] * width for width, val in zip(widths, vals)])) for widths, vals in zip(width_normalised, sampled_values)]
decoded_sym = [[2.0 * (elem - 0.5 ) for elem in lst] for lst in decoded]

corrs = [si.correlate(seq, preamble_sym) for seq in decoded_sym]
correct_preamble = [(decoded[idx], list(corr).index(len(preamble))) for idx, corr in enumerate(corrs) if max(corr) >= len(preamble)]
correct_preamble_idx = [idx for idx, corr in enumerate(corrs) if max(corr) >= len(preamble)]
full_payload = [seq[idx + 1:idx + 1 + payload_len - 1] for seq, idx in correct_preamble if idx + 1 + payload_len <= len(seq)]

plt.plot(waves[0])
plt.show()

#print(correct_preamble_idx)
"""
for correct in full_payload:
    plt.plot([0] + correct)
    plt.show()
"""