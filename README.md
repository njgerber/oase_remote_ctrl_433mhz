# Understanding the Oase FM Master 1

## Introduction

This project was about hacking around with the remote control from [Oase](https://www.oase-teichbau.de/handsender-fm-master-1-3) operating in the 433MHz ISM band. The main motivation was for home motivation purpose to automate tasks to understand how the remote control works as Oase does not provide useful information for this.

## Tools

To do this project I used an RF analiser (such as the [RfCat](https://int3.cc/products/rfcat)) and an oscilloscope (a logic analiser would work as well).

## The PCB

Besides various passives, transistors and dip switches, the two main components of the PCB are a little PIC controller and a SAW oscillator. Probing around with the scope what happens when we press a button we see that it is one output of the PIC which provides the modulation signals (all other pin do the same no matter which button was pressed).

### Baseband Modulation Signal Recording

Probing the pin and pushing the button we can find the baseband signals defining the different buttons:

![Button 1](scope_plots/11.png "Button 1")

The plots of the other buttons can be found under [plots](plots).

<!--
![Button 2](scope_plots/12.png "Button 2")
![Button 3](scope_plots/13.png "Button 3")
![Button 4](scope_plots/14.png "Button 4")

For the second half of the plots the initial falling edge was ommited (plus the colors were inverted).

![Button 5](scope_plots/21.png "Button 5")
![Button 6](scope_plots/22.png "Button 6")
![Button 7](scope_plots/23.png "Button 7")
![Button 8](scope_plots/24.png "Button 8")
-->

Comparing the different plots we see that in any case when pushing a button, the output value falls from roughly 1V to 0V and stay there for a while where then a preamble is sent.

![Transission Start](plots/preamble.png "Transission Start")

After the preamble there comes a kind of one hot code which depends on the switch (which pulse of the incoming 8 pulse has double the width defines which button was pressed).

## RF Signal Recording

The recorded baseband signals look similar to a Manchester encoding, however a detailed analysis reveals that it is not possible to define a clock signal (which is close to the symbol rate) which assures that there is always a transition on the baseband signal on the active edge. Therefore, our first guess is to check for ASK or OOK signals. As we are not quite sure about the exact symbol rate of the signal and how it looks like in RF, we oversample it as much as possible. On our RfCat the maximal baudrate for ASK/OOk is 25k.
